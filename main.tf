terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
  profile = "sandbox.terraform.service.account"
}

resource "aws_s3_bucket" "bucket_test" {
  bucket = "a-cool-name"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
