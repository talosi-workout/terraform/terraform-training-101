# terraform-training-101


## Prerequisites
1 install aws cli
2 install terrafom


## Setup

first configure your aws cli with
```sh
aws configure
```

In your file ~/.aws/credentials add the pattern

```txt
[sandbox.terraform.service.account]
aws_access_key_id = <Replace me by your acces key>
aws_secret_access_key = <Replace me by your secret key>
region = eu-central-1
```

## Terraform cli
```sh
terraform init
terraform plan
terraform apply
```

After you practice run
```sh
terraform destroy
```

## Check bucket created
```sh
aws s3 ls --profile sandbox.terraform.service.account
```